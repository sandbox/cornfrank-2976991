<?php

namespace Drupal\daterange_plus\Plugin\views\filter;

/**
 * Date filter handler for date range fields.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("daterange_plus_date")
 */
class DateRangePlusFilterDate extends DateRangePlusFilterBase {

}
