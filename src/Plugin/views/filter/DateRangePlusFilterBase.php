<?php

namespace Drupal\daterange_plus\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\daterange_plus\DateRangePlusHelper;
use Drupal\datetime\Plugin\views\filter\Date;

/**
 * Base for the date range views filters.
 */
abstract class DateRangePlusFilterBase extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    return $options;
  }

  /**
   * Add a type selector to the value form
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);
    if ($form_state->get('exposed')) {
      $form['value'] = [
        '#title' => $this->t('Date'),
        '#type' => 'date',
      ];
    }
    else {
      $form['value'] = [
        '#title' => $this->t('Date'),
        '#type' => 'textfield',
        '#size' => 30,
        '#default_value' => $this->value,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $field = "$this->tableAlias.$this->realField";
    $date_strs = is_array($this->value)
      ? DateRangePlusHelper::parseDateArg(reset($this->value))
      : DateRangePlusHelper::parseDateArg($this->value);
    $this->query->addWhere($this->options['group'], $field,
      $date_strs, 'BETWEEN');
  }

}
