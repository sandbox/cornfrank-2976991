<?php

namespace Drupal\daterange_plus\Plugin\views\argument;


/**
 * Date argument handler for date range fields.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("daterange_plus_date")
 */

class DateRangePlusArgumentDate extends DateRangePlusArgumentBase {

}
