<?php

namespace Drupal\daterange_plus\Plugin\views\argument;

use Drupal\views\Plugin\views\argument\NumericArgument;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\daterange_plus\DateRangePlusHelper;

/**
 * Base class for daterange_plus argument handlers.
 */
abstract class DateRangePlusArgumentBase extends ArgumentPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    $this->ensureMyTable();
    $field = "$this->tableAlias.$this->realField";

    $date_strs = DateRangePlusHelper::parseDateArg($this->argument);
    $this->query->addWhere(0, $field, $date_strs, 'BETWEEN');
  }

}
