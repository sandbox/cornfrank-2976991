<?php

namespace Drupal\daterange_plus\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\daterange_compact\Plugin\Field\FieldFormatter\DateRangeCompactFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Plugin implementation of the 'daterange_plus_partial' formatter for 'daterange' fields.
 *
 * Extends the date range compact formatter to allow partial date display and
 * to ignore the timezone.
 *
 * @FieldFormatter(
 *   id = "daterange_plus_partial",
 *   label = @Translation("Partial date or time"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class DateRangePlusPartialFormatter extends DateRangeCompactFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'partial' => 'none',
    ] + parent::defaultSettings();
  }

  /**
   * Return the partial display options.
   *
   * @return array
   */
  protected function partialOptions() {
    return [
      'none' => $this->t('Date and time'),
      'date' => $this->t('Date only'),
      'time' => $this->t('Time only'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['partial'] = [
      '#title' => $this->t('Display options'),
      '#type' => 'select',
      '#options' => $this->partialOptions(),
      '#default_value' => $this->getSetting('partial'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $partial = $this->getSetting('partial');
    if ($partial != 'none') {
      $summary[] = $this->t('Display @partial.',
        ['@partial' => strtolower($this->partialOptions()[$partial])]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      if (!empty($item->start_date) && !empty($item->end_date)) {
        $start_timestamp = $item->start_date->getTimestamp();
        $end_timestamp = $item->end_date->getTimestamp();
        $format = $this->getSetting('format_type');

        // Date only field type or date only display.
        if ($this->getFieldSetting('datetime_type') == DateTimeItem::DATETIME_TYPE_DATE) {
          $timezone = DATETIME_STORAGE_TIMEZONE;
          $text = $this->dateRangeFormatter->formatDateRange(
            $start_timestamp, $end_timestamp, $format, $timezone);
        }
        elseif ($this->getSetting('partial') == 'date') {
          $timezone = drupal_get_user_timezone();
          $text = $this->dateRangeFormatter->formatDateRange(
            $start_timestamp, $end_timestamp, $format, $timezone);
        }
        else {
          // Set the text for date and time display.
          $timezone = drupal_get_user_timezone();
          $text = $this->dateRangeFormatter->formatDateTimeRange(
            $start_timestamp, $end_timestamp, $format, $timezone);
          // If we only want the time, then strip out the date part.
          if ($this->getSetting('partial') == 'time') {
            $date_part = $this->dateRangeFormatter->formatDateRange(
              $start_timestamp, $end_timestamp, $format, $timezone);
            $text = trim(str_replace($date_part, '', $text));
          }
        }

        $elements[$delta] = [
          '#plain_text' => $text,
          '#cache' => [
            'contexts' => [
              'timezone',
            ],
          ],
        ];
      }
    }

    return $elements;
  }

}
