<?php

namespace Drupal\daterange_plus\Plugin\Field\FieldWidget;

use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeDefaultWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'daterange_oneday' widget.
 *
 * @FieldWidget(
 *   id = "daterange_plus_oneday",
 *   label = @Translation("Single date and time range"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class DateRangePlusOneDayWidget extends DateRangeDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta,
      array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Move all form elements inline.
    $element['#attributes']['class'][] = 'form--inline';
    $element['#attributes']['class'][] = 'clearfix';

    // Remove the unwanted titles.
    $element['value']['#title'] = '';
    $element['end_value']['#title'] = '';
    // Remove the end date element.
    $element['end_value']['#date_date_element'] = 'none';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Copy the start date to the end date.
    foreach ($values as $delta => $value) {
      /* @var $start_date \Drupal\Core\Datetime\DrupalDateTime */
      /* @var $end_date \Drupal\Core\Datetime\DrupalDateTime */
      $start_date = $value['value'];
      list($year, $month, $day) = explode('-', $start_date->format('Y-m-d'));
      $end_date = $value['end_value'];
      $end_date->setDate((int) $year, (int) $month, (int) $day);
      $values[$delta]['end_value'] = $end_date;
    }

    return parent::massageFormValues($values, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateStartEnd(array &$element, FormStateInterface $form_state, array &$complete_form) {
    // No need to validate the end date as we set it later.
  }

}
