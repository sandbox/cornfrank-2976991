<?php

namespace Drupal\daterange_plus;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Utility helper functions for date range plus.
 */
class DateRangePlusHelper {

  /**
   * Parse the value passed to the date argument/filters.
   *
   * This will either be a single date string, or two date strings separated
   * by a double colon ('::').
   *
   * @param string $argument
   * @return array
   *  Array of date strings to use in the where statements.
   */
  public static function parseDateArg($argument) {
    $arguments = explode('::', $argument);
    if (empty($arguments[1])) {
      $arguments[1] = $arguments[0];
    }
    $date_strs = [];
    $date = new DrupalDateTime($arguments[0]);
    $date->setTime(0, 0, 0);
    $date_strs[] = $date->format(DATETIME_DATETIME_STORAGE_FORMAT,
        ['timezone' => DATETIME_STORAGE_TIMEZONE]);

    $date = new DrupalDateTime($arguments[1]);
    $date->setTime(23, 59, 59);
    $date_strs[] = $date->format(DATETIME_DATETIME_STORAGE_FORMAT,
        ['timezone' => DATETIME_STORAGE_TIMEZONE]);

    return $date_strs;
  }

}
