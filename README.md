# Introduction

Enhancements to the daterange field.

# Requirements
- daterange
- daterange_compact

# Views
- Filter and argument handlers to filter by start date or end date. These take
  a single valid date string ('today', '2018-06-30', etc) or two dates
  separated by a double colon (eg 'today::tomorrow') for a date range.

# Fields
- A widget to enter a single date with a time range.
- A field formatter to display either the date or the time part of the field.
  Requires the daterange_compact module.
