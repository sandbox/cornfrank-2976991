<?php

/**
 * @file
 * Provides views data for the daterange_plus module.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data_alter().
 */
function daterange_plus_field_views_data_alter(array &$data, FieldStorageConfigInterface $field_storage) {

  if ($field_storage->getType() == 'daterange') {
    $field_name = $field_storage->getName();
    $entity_type_id = $field_storage->getTargetEntityTypeId();
    $table_name = $entity_type_id . '__' . $field_name;
    $value_column = $field_name . '_value';
    $end_value_column = $field_name . '_end_value';

    $group = $data[$table_name][$value_column]['group'];

    $start_date_filter = $data[$table_name][$value_column]['filter'];
    $start_date_filter['id'] = 'daterange_plus_date';
    $start_date_argument = $data[$table_name][$value_column]['argument'];
    $start_date_argument['id'] = 'daterange_plus_date';

    $data[$table_name]['daterange_plus_start_date'] = [
      'group' => $group,
      'title' => t('Start date (daterange_plus)'),
      'help' => t('Filter for the start date.'),
      'filter' => $start_date_filter,
      'argument' => $start_date_argument,
    ];

    $end_date_filter = $data[$table_name][$end_value_column]['filter'];
    $end_date_filter['id'] = 'daterange_plus_date';
    $end_date_argument = $data[$table_name][$end_value_column]['argument'];
    $end_date_argument['id'] = 'daterange_plus_date';

    $data[$table_name]['daterange_plus_end_date'] = [
      'group' => $group,
      'title' => t('End date (daterange_plus)'),
      'help' => t('Filter for the end date.'),
      'filter' => $end_date_filter,
      'argument' => $end_date_argument,
      'real_field' => $end_value_column,
    ];

  }

}
